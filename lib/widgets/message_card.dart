import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:we_chat/api/apis.dart';
import 'package:we_chat/helper/dialogs.dart';
import 'package:we_chat/helper/my_date_util.dart';
import 'package:we_chat/main.dart';
import 'package:we_chat/models/message.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:open_file/open_file.dart';

class MessageCard extends StatefulWidget {
  const MessageCard({Key? key, required this.message}) : super(key: key);

  final Message message;

  @override
  State<MessageCard> createState() => _MessageCardState();
}

class _MessageCardState extends State<MessageCard> {
  late Future<Directory?> mailchatDirectoryFuture;

  @override
  void initState() {
    super.initState();
    mailchatDirectoryFuture = getMailchatDirectory();
  }

  Future<Directory?> getMailchatDirectory() async {
    final externalDir = await getExternalStorageDirectory();
    final mailchatDirectory =
        Directory('${externalDir!.path}/Mailchat document');

    if (!await mailchatDirectory.exists()) {
      await mailchatDirectory.create(recursive: true);
    }

    return mailchatDirectory;
  }

  void _handleDownloadFile(String fileUrl) async {
    if (fileUrl != null && fileUrl.isNotEmpty) {
      // Minta izin MANAGE_EXTERNAL_STORAGE
      var status = await Permission.manageExternalStorage.request();
      if (status.isGranted) {
        final mailchatDirectory = await mailchatDirectoryFuture;
        if (mailchatDirectory != null) {
          final Dio dio = Dio();
          final fileName = widget.message.fileName ?? 'File';
          final file = File('${mailchatDirectory.path}/$fileName');

          try {
            final response = await dio.download(
              fileUrl,
              file.path,
              onReceiveProgress: (received, total) {
                if (total != -1) {
                  double progress = (received / total) * 100;
                  print('Download progress: ${progress.toStringAsFixed(2)}%');
                }
              },
            );

            // Update status isDownloaded di Firestore
            await APIs.updateMessageIsDownloaded(widget.message);

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('File berhasil diunduh'),
            ));

            OpenFile.open(file.path);
          } catch (e) {
            print('Error while downloading file: $e');
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text('Terjadi kesalahan saat mengunduh file.'),
            ));
          }
        }
      } else {
        print('Izin MANAGE_EXTERNAL_STORAGE tidak diberikan.');
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Izin MANAGE_EXTERNAL_STORAGE tidak diberikan.'),
        ));
      }
    } else {
      print('URL file kosong atau null.');
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('URL file kosong atau null.'),
      ));
    }
  }

  void _openFileWithThirdPartyApp() async {
    final mailchatDirectory = await mailchatDirectoryFuture;
    final fileName = widget.message.fileName ?? 'File';
    final file = File('${mailchatDirectory?.path}/$fileName');
    OpenFile.open(file.path);
  }

  @override
  Widget build(BuildContext context) {
    bool isMe = APIs.user.uid == widget.message.fromId;
    return InkWell(
      onLongPress: () {
        _showBottomSheet(isMe);
      },
      child: isMe ? _greenMessage() : _blueMessage(),
    );
  }

 Widget _blueMessage() {
    if (widget.message.read.isEmpty) {
      APIs.updateMessageReadStatus(widget.message);
    }
    return FutureBuilder<Directory?>(
      future: mailchatDirectoryFuture,
      builder: (context, snapshot) {
        final mq = MediaQuery.of(context);
        return InkWell(
          onTap: () {
            if (widget.message.type == Type.image) {
              _showImageFullScreen(context, widget.message.msg);
            } else {
              // Tindakan khusus untuk pesan non-gambar.
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 5.0),
              Container(
                padding: EdgeInsets.all(
                  widget.message.type == Type.image
                      ? mq.size.width * 0.03
                      : mq.size.width * 0.04,
                ),
                margin: EdgeInsets.only(
                  left: mq.size.width * 0.04,
                  right: mq.size.width * 0.04,
                  bottom: 4.0,
                ),
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 221, 245, 255),
                  border:
                      Border.all(color: const Color.fromARGB(255, 0, 95, 139)),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (widget.message.type == Type.text)
                      Text(
                        widget.message.msg,
                        style: const TextStyle(
                            fontSize: 15, color: Colors.black87),
                      )
                    else if (widget.message.type == Type.file)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (widget.message.isDownloaded)
                            InkWell(
                              onTap: () {
                                _openFileWithThirdPartyApp();
                              },
                              child: Row(
                                children: [
                                  Icon(Icons.insert_drive_file,
                                      color: Colors.blue, size: 24),
                                  SizedBox(width: 8),
                                  Flexible(
                                    child: Text(
                                      widget.message.fileName ?? 'File',
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black87),
                                      overflow: TextOverflow.visible,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          else
                            Row(
                              children: [
                                Icon(Icons.insert_drive_file,
                                    color: Colors.blue, size: 24),
                                SizedBox(width: 8),
                                Flexible(
                                  child: Text(
                                    widget.message.fileName ?? 'File',
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.black87),
                                    overflow: TextOverflow.visible,
                                  ),
                                ),
                              ],
                            ),
                          SizedBox(height: 2),
                          if (!widget.message
                              .isDownloaded) // Tampilkan ikon "Unduh" hanya jika belum diunduh
                            InkWell(
                              onTap: () {
                                _handleDownloadFile(widget.message.msg);
                              },
                              child: Row(
                                children: [
                                  Icon(Icons.download_rounded,
                                      color: Colors.blue, size: 20),
                                  SizedBox(width: 4),
                                  Text(
                                    'Unduh',
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.blue),
                                  ),
                                ],
                              ),
                            ),
                        ],
                      )
                    else if (widget.message.type == Type.image)
                      CachedNetworkImage(
                        imageUrl: widget.message.msg, // URL gambar
                        placeholder: (context, url) =>
                            CircularProgressIndicator(), // Placeholder selama gambar dimuat
                        errorWidget: (context, url, error) => Icon(
                            Icons.error), // Widget ketika gagal memuat gambar
                      ),
                  ],
                ),
              ),
              SizedBox(height: 2.0),
              Padding(
                padding: EdgeInsets.only(left: mq.size.width * 0.04),
                child: Text(
                  MyDateUtil.getFormattedTime(
                    context: context,
                    time: widget.message.sent,
                  ),
                  style: const TextStyle(fontSize: 13, color: Colors.black54),
                ),
              ),
            ],
          ),
        );
      },
    );
  }


 Widget _greenMessage() {
    if (widget.message.read.isEmpty) {
      APIs.updateMessageReadStatus(widget.message);
    }
    final mq = MediaQuery.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Flexible(
              child: Container(
                padding: EdgeInsets.all(
                  widget.message.type == Type.image
                      ? mq.size.width * 0.03
                      : mq.size.width * 0.04,
                ),
                margin: EdgeInsets.symmetric(
                  horizontal: mq.size.width * 0.04,
                  vertical: mq.size.height * 0.01,
                ),
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 218, 255, 176),
                  border: Border.all(color: Colors.lightGreen),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    if (widget.message.type == Type.text)
                      Text(
                        widget.message.msg,
                        style: const TextStyle(
                          fontSize: 15,
                          color: Colors.black87,
                        ),
                      )
                    else if (widget.message.type == Type.file)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.insert_drive_file,
                            color: Colors.blue,
                            size: 24,
                          ),
                          SizedBox(width: 8),
                          Flexible(
                            child: Text(
                              widget.message.fileName ?? 'File',
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.black87,
                              ),
                              overflow: TextOverflow.visible,
                            ),
                          ),
                        ],
                      )
                  else if (widget.message.type == Type.image)
                      Hero(
                        tag: widget.message.msg, 
                        child: InkWell(
                          onTap: () {
                            _showImageFullScreen(context, widget.message.msg);
                          },
                          child: Padding(
                            padding: EdgeInsets.all(
                              mq.size.width * 0.03,
                            ),
                            child: Image.network(
                              widget.message.msg,
                              width: mq.size.width *
                                  0.6, 
                              height: mq.size.height * 0.4,
                            ),
                          ),
                        ),
                      ),


                    SizedBox(height: 4),
                  ],
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              if (widget.message.read.isNotEmpty)
                Icon(
                  Icons.done_all_rounded,
                  color: Colors.blue,
                  size: 20,
                )
              else
                Icon(
                  Icons.done_all_rounded,
                  color: Colors.grey,
                  size: 20,
                ),
              SizedBox(width: 2),
              Text(
                MyDateUtil.getFormattedTime(
                  context: context,
                  time: widget.message.sent,
                ),
                style: const TextStyle(
                  fontSize: 13,
                  color: Colors.black54,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void _showImageFullScreen(BuildContext context, String imageUrl) {
    Navigator.of(context).push(PageRouteBuilder<void>(
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return Material(
          color: Colors.black,
          child: InkWell(
            onTap: () {
              Navigator.of(context)
                  .pop(); // Kembali ke halaman chat screen saat gambar diklik
            },
            child: SizedBox.expand(
              child: Hero(
                tag:
                    imageUrl, // Gunakan tag yang sama dengan yang ada di _greenMessage
                child: Image.network(
                  imageUrl,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
        );
      },
    ));
  }


  void _showBottomSheet(bool isMe) {
    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      builder: (_) {
        return ListView(
          shrinkWrap: true,
          children: [
            Container(
              height: 4,
              margin: EdgeInsets.symmetric(
                vertical: mq.height * .015,
                horizontal: mq.width * .4,
              ),
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            if (widget.message.type == Type.text)
              _OptionItem(
                icon: const Icon(Icons.copy_all_rounded,
                    color: Colors.blue, size: 26),
                name: 'Copy Text',
                onTap: () async {
                  await Clipboard.setData(
                    ClipboardData(text: widget.message.msg),
                  ).then((value) {
                    Navigator.pop(context);
                    Dialogs.showSnackBar(context, 'Text Copied!');
                  });
                },
              )
            else
              _OptionItem(
                icon: const Icon(Icons.download_rounded,
                    color: Colors.blue, size: 26),
                name: 'Save Image',
                onTap: () async {
                  try {
                    await GallerySaver.saveImage(
                      widget.message.msg,
                      albumName: 'We Chat',
                    ).then((success) {
                      Navigator.pop(context);
                      if (success != null && success) {
                        Dialogs.showSnackBar(
                          context,
                          'File Successfully Saved!',
                        );
                      }
                    });
                  } catch (e) {
                    print('ErrorWhileSavingFile: $e');
                  }
                },
              ),
            Divider(
              color: Colors.black54,
              endIndent: mq.width * .04,
              indent: mq.width * .04,
            ),
            if (isMe && widget.message.type == Type.text)
              _OptionItem(
                icon: const Icon(Icons.edit, color: Colors.blue, size: 26),
                name: 'Edit Message',
                onTap: () {
                  Navigator.pop(context);
                  _showMessageUpdateDialog();
                },
              ),
            if (isMe)
              _OptionItem(
                icon: const Icon(Icons.delete_forever,
                    color: Colors.red, size: 26),
                name: 'Delete Message',
                onTap: () async {
                  await APIs.deleteMessage(widget.message).then((value) {
                    Navigator.pop(context);
                  });
                },
              ),
            Divider(
              color: Colors.black54,
              endIndent: mq.width * .04,
              indent: mq.width * .04,
            ),
            _OptionItem(
              icon: const Icon(Icons.remove_red_eye, color: Colors.blue),
              name:
                  'Sent At: ${MyDateUtil.getMessageTime(context: context, time: widget.message.sent)}',
              onTap: () {},
            ),
            _OptionItem(
              icon: const Icon(Icons.remove_red_eye, color: Colors.green),
              name: widget.message.read.isEmpty
                  ? 'Read At: Not seen yet'
                  : 'Read At: ${MyDateUtil.getMessageTime(context: context, time: widget.message.read)}',
              onTap: () {},
            ),
          ],
        );
      },
    );
  }

  void _showMessageUpdateDialog() {
    String updatedMsg = widget.message.msg;

    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        contentPadding: const EdgeInsets.only(
          left: 24,
          right: 24,
          top: 20,
          bottom: 10,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        title: Row(
          children: const [
            Icon(
              Icons.message,
              color: Colors.blue,
              size: 28,
            ),
            Text(' Update Message'),
          ],
        ),
        content: TextFormField(
          initialValue: updatedMsg,
          maxLines: null,
          onChanged: (value) => updatedMsg = value,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
        actions: [
          MaterialButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text(
              'Cancel',
              style: TextStyle(
                color: Color.fromARGB(255, 10, 117, 204),
                fontSize: 16,
              ),
            ),
          ),
          MaterialButton(
            onPressed: () {
              Navigator.pop(context);
              APIs.updateMessage(widget.message, updatedMsg);
            },
            child: const Text(
              'Update',
              style: TextStyle(
                color: Color.fromARGB(255, 10, 117, 204),
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _OptionItem extends StatelessWidget {
  final Icon icon;
  final String name;
  final VoidCallback onTap;

  const _OptionItem({
    required this.icon,
    required this.name,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(),
      child: Padding(
        padding: EdgeInsets.only(
          left: mq.width * .05,
          top: mq.height * .015,
          bottom: mq.height * .015,
        ),
        child: Row(
          children: [
            icon,
            Flexible(
              child: Text(
                '    $name',
                style: const TextStyle(
                  fontSize: 15,
                  color: Colors.black54,
                  letterSpacing: 0.5,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

