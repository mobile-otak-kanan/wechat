import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:we_chat/api/apis.dart';
import 'package:we_chat/main.dart';
import 'package:we_chat/models/chat_user.dart';
import 'package:we_chat/models/message.dart';
import 'package:we_chat/screens/chat_screen.dart';
import 'package:we_chat/widgets/dialogs/profil_dialog.dart';

class ChatUserCard extends StatefulWidget {
  final ChatUser user;
  final bool isSelected;

  const ChatUserCard({Key? key, required this.user, this.isSelected = false})
      : super(key: key);

  @override
  State<ChatUserCard> createState() => _ChatUserCardState();
}

class _ChatUserCardState extends State<ChatUserCard> {
  bool _lastMessageFromMe = false;
  bool _messageRead = false;
  Message? _message;
  int _unreadMessageCount = 0;
  late StreamSubscription<QuerySnapshot> _messageSubscription;
  List<ChatUser> _chatUsers = [];

  void _showDeleteConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Hapus Percakapan"),
          content: Text("Anda yakin ingin menghapus percakapan ini?"),
          actions: <Widget>[
            TextButton(
              child: Text("Batal"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Hapus"),
              onPressed: () {
                _deleteConversation(widget.user);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _deleteConversation(ChatUser user) {
    // Menghapus percakapan dari Firestore
    APIs.deleteConversation(user);

    // Menghapus pesan dari Firestore
    APIs.deleteMessages(user);

     // Hapus ChatUser dari _chatUsers
    _removeChatUser(widget.user);

    // Rebuild ListView
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _getLastMessage();
  }

  void _addOrUpdateChatUser(ChatUser user) {
    final index = _chatUsers.indexOf(user);
    if (index != -1) {
      _chatUsers.removeAt(index);
    }
    _chatUsers.insert(0, user);
  }

  void _removeChatUser(ChatUser user) {
    final index = _chatUsers.indexWhere((chatUser) => chatUser.id == user.id);
    if (index != -1) {
      setState(() {
        _chatUsers.removeAt(index);
      });
    }
  }

  void _getLastMessage() async {
    _messageSubscription =
        APIs.getLastMessage(widget.user).listen((lastMessageSnapshot) {
      if (lastMessageSnapshot != null) {
        final lastMessageData =
            lastMessageSnapshot.docs.first.data() as Map<String, dynamic>;
        final newMessage = Message.fromJson(lastMessageData);

        setState(() {
          _message = newMessage;
          _lastMessageFromMe = _message?.fromId == APIs.user.uid;
          _messageRead = _message?.read?.isNotEmpty == true;

          if (!_lastMessageFromMe && !_messageRead) {
            _unreadMessageCount++;
          } else {
            _unreadMessageCount = 0;
          }

          if (_unreadMessageCount == 0) {
            _removeChatUser(widget.user);
          } else {
            _addOrUpdateChatUser(widget.user);
          }
        });
      }
    });
  }

  @override
  void dispose() {
    _messageSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool _isUnread = !_lastMessageFromMe && !_messageRead;

   return Card(
      margin: EdgeInsets.symmetric(horizontal: mq.width * .04, vertical: 4),
      elevation: 0.5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ChatScreen(user: widget.user),
            ),
          );
        },
        onLongPress: () {
          _showDeleteConfirmationDialog(context);
        },

        child: ListTile(
          leading: InkWell(
            onTap: () {
              showDialog(
                context: context,
                builder: (_) => ProfileDialog(user: widget.user),
              );
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(mq.height * .03),
              child: CachedNetworkImage(
                width: mq.height * .055,
                height: mq.height * .055,
                imageUrl: widget.user.image,
                errorWidget: (context, url, error) =>
                    const CircleAvatar(child: Icon(CupertinoIcons.person)),
              ),
            ),
          ),
          title: Row(
            children: [
              Text(
                widget.user.name,
                style: TextStyle(
                  fontWeight: _isUnread ? FontWeight.bold : FontWeight.normal,
                ),
              ),
              if (_unreadMessageCount > 0)
                Container(
                  margin: EdgeInsets.only(left: 5),
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    shape: BoxShape.circle,
                  ),
                  child: Text(
                    '$_unreadMessageCount',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
            ],
          ),
          subtitle: Row(
            children: [
              _lastMessageFromMe
                  ? _messageRead
                      ? Icon(
                          Icons.done_all_rounded,
                          color: Colors.blue,
                          size: 20,
                        )
                      : Icon(
                          Icons.done_all_rounded,
                          color: Colors.grey,
                          size: 20,
                        )
                  : Container(),
              SizedBox(width: 6),
              Expanded(
                child: _message != null
                    ? _message!.type == Type.file
                        ? Text(
                            _message!.fileName ?? 'File',
                            overflow: TextOverflow.ellipsis,
                          )
                        : _message!.type == Type.image
                            ? Row(
                                children: [
                                  Icon(
                                    Icons.image_rounded,
                                    color: Colors.blue,
                                    size: 20,
                                  ),
                                  SizedBox(width: 6),
                                  Text(
                                    'Image',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontWeight: _isUnread
                                          ? FontWeight.bold
                                          : FontWeight.normal,
                                    ),
                                  ),
                                ],
                              )
                            : Text(
                                _message!.msg,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontWeight: _isUnread
                                      ? FontWeight.bold
                                      : FontWeight.normal,
                                ),
                              )
                    : Text(widget.user.about),
              ),
            ],
          ),

        ),
      ),
    );
  }
}
