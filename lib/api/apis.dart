import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:http/http.dart';
import 'package:we_chat/models/chat_user.dart';
import 'package:we_chat/models/chatmember.dart';
import 'package:we_chat/models/message.dart';
import 'package:we_chat/screens/addmember_screen.dart';

class APIs {
  static FirebaseAuth auth = FirebaseAuth.instance;

  static FirebaseFirestore firestore = FirebaseFirestore.instance;

  static FirebaseStorage storage = FirebaseStorage.instance;

  static ChatUser me = ChatUser(
      id: user.uid,
      name: user.displayName.toString(),
      email: user.email.toString(),
      about: "Hey, I'm using We Chat!",
      image: user.photoURL.toString(),
      createdAt: '',
      isOnline: false,
      lastActive: '',
      pushToken: '');

  static get user => auth.currentUser!;

  static FirebaseMessaging fMessaging = FirebaseMessaging.instance;

  // for getting firebase messaging token
  static Future<void> getFirebaseMessagingToken() async {
      await fMessaging.requestPermission();

    await fMessaging.getToken().then((t) {
      if (t != null) {
        me.pushToken = t;
        log('Push Token: $t');
      }
    });

    // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    //   log('Got a message whilst in the foreground!');
    //   log('Message data: ${message.data}');

    //   if (message.notification != null) {
    //     log('Message also contained a notification: ${message.notification}');
    //   }
    // });
  }

  static Future<void> sendPushNotification(
      ChatUser chatUser, String msg) async {
    try {
      final body = {
        "to": chatUser.pushToken,
        "notification": {
          "title": me.name, //our name should be send
          "body": msg,
          "android_channel_id": "chats"
        },
        "data": {
          "some_data": "User ID: ${me.id}",
        },
      };

      var res = await post(Uri.parse('https://fcm.googleapis.com/fcm/send'),
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader:
                'key=AAAAqGP8Jac:APA91bHpFDAqyMhY91NFvzzqIEvlV9sKg0kE3Xaw-jECLEzEw8RRYvdNUkJHH49YGawNKZNsPlV4muaaqsORwmKRmHF9jWSWXQxZx4nE22BXNmnzrKZYaukiSKZRhu26i_NzRke-MHPi'
          },
          body: jsonEncode(body));
      log('Response status: ${res.statusCode}');
      log('Response body: ${res.body}');
    } catch (e) {
      log('\nsendPushNotificationE: $e');
    }
  }


  static Future<bool> userExists() async {
    return (await firestore.collection('users').doc(user.uid).get()).exists;
  }

  // for adding an chat user for our conversation
  static Future<bool> addChatUser(String email) async {
    final data = await firestore
        .collection('users')
        .where('email', isEqualTo: email)
        .get();

    log('data: ${data.docs}');

    if (data.docs.isNotEmpty && data.docs.first.id != user.uid) {
      //user exists

      log('user exists: ${data.docs.first.data()}');

      firestore
          .collection('users')
          .doc(user.uid)
          .collection('my_users')
          .doc(data.docs.first.id)
          .set({});

      return true;
    } else {
      //user doesn't exists

      return false;
    }
  }

  // for getting current user info
  static Future<void> getSelfInfo() async {
    await firestore.collection('users').doc(user.uid).get().then((user) async {
      if (user.exists) {
        me = ChatUser.fromJson(user.data()!);
        await getFirebaseMessagingToken();

        //for setting user status to active
         APIs.updateActiveStatus(true);
        log('My Data: ${user.data()}');
      } else {
        await createUser().then((value) => getSelfInfo());
      }
    });
  }

  static Future<void> createUser() async {
    final time = DateTime.now().microsecondsSinceEpoch.toString();
    final chatUser = ChatUser(
        id: user.uid,
        name: user.displayName.toString(),
        email: user.email.toString(),
        about: "Hey. I'm using Mail Chat",
        image: user.photoURL.toString(),
        createdAt: time,
        isOnline: false,
        lastActive: time,
        pushToken: '');

    return await firestore
        .collection('users')
        .doc(user.uid)
        .set(chatUser.toJson());
  }

   // for getting id's of known users from firestore database
  static Stream<QuerySnapshot<Map<String, dynamic>>> getMyUsersId() {
    return firestore
        .collection('users')
        .doc(user.uid)
        .collection('my_users')
        .snapshots();
  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> getAllUsers(
      List<String> userIds) {
    log('\nUserIds: $userIds');

    return firestore
        .collection('users')
        .where('id',
            whereIn: userIds.isEmpty
                ? ['']
                : userIds) //because empty list throws an error
        // .where('id', isNotEqualTo: user.uid)
        .snapshots();
  }

   static Future<void> sendFirstMessage(
      ChatUser chatUser, String msg, Type type) async {
    await firestore
        .collection('users')
        .doc(chatUser.id)
        .collection('my_users')
        .doc(user.uid)
        .set({}).then((value) => sendMessage(chatUser, msg, type));
  }

  static Future<void> updateUserInfo() async {
    await firestore.collection('users').doc(user.uid).update({
      'name': me.name,
      'about': me.about,
    });
  }

  static Future<void> updateProfilePicture(File file) async {
    //getting image file extension
    final ext = file.path.split('.').last;
    log('Extension: $ext');

    //storage file ref with path
    final ref = storage.ref().child('profile_pictures/${user.uid}.$ext');

    //uploading image
    await ref
        .putFile(file, SettableMetadata(contentType: 'image/$ext'))
        .then((p0) {
      log('Data Transferred: ${p0.bytesTransferred / 1000} kb');
    });

    //updating image in firestore database
    me.image = await ref.getDownloadURL();
    await firestore
        .collection('users')
        .doc(user.uid)
        .update({'image': me.image});
  }

  static String getConversationID(String id) => user.uid.hashCode <= id.hashCode
      ? '${user.uid}_$id'
      : '${id}_${user.uid}';

  // for getting all messages of a specific conversation from firestore database
  static Stream<QuerySnapshot<Map<String, dynamic>>> getAllMessages(
      ChatUser user) {
    return firestore
        .collection('chats/${getConversationID(user.id)}/messages/')
        .orderBy('sent', descending: true)
        .snapshots();
  }

 static Future<void> sendMessage(ChatUser chatUser, String msg, Type type,
      {String? filename}) async {
    // Message sending time (also used as id)
    final time = DateTime.now().millisecondsSinceEpoch.toString();

    // Message to send
    final Message message = Message(
      toId: chatUser.id,
      msg: msg,
      read: '',
      type: type,
      fromId: user.uid,
      sent: time,
      fileName: filename,
      isDownloaded: false,
    );

    final ref = firestore
        .collection('chats/${getConversationID(chatUser.id)}/messages/');
    await ref.doc(time).set(message.toJson()).then((value) =>
        sendPushNotification(chatUser, type == Type.text ? msg : 'file'));
  }

static Future<void> updateMessageIsDownloaded(Message message) async {
    try {
      final collectionPath =
          'chats/${getConversationID(message.fromId)}/messages';
      final documentPath = '$collectionPath/${message.sent}';

      final docRef = firestore.doc(documentPath);
      final docSnapshot = await docRef.get();

      if (!docSnapshot.exists) {
        print('Dokumen tidak ditemukan: $documentPath');
        return;
      }

      await docRef.update({'isDownloaded': true});
      print('Status isDownloaded berhasil diperbarui.');
    } catch (e) {
      print('Gagal memperbarui status isDownloaded: $e');
    }
  }

  static Future<void> updateMessageReadStatus(Message message) async {
    firestore
        .collection('chats/${getConversationID(message.fromId)}/messages/')
        .doc(message.sent)
        .update({'read': DateTime.now().millisecondsSinceEpoch.toString()});
  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> getLastMessage(
      ChatUser user) {
    return firestore
        .collection('chats/${getConversationID(user.id)}/messages/')
        .orderBy('sent', descending: true)
        .limit(1)
        .snapshots();
  }

  static Future<void> sendChatImage(ChatUser chatUser, File file) async {
    //getting image file extension
    final ext = file.path.split('.').last;

    //storage file ref with path
    final ref = storage.ref().child(
        'images/${getConversationID(chatUser.id)}/${DateTime.now().millisecondsSinceEpoch}.$ext');

    //uploading image
    await ref
        .putFile(file, SettableMetadata(contentType: 'image/$ext'))
        .then((p0) {
      log('Data Transferred: ${p0.bytesTransferred / 1000} kb');
    });

    //updating image in firestore database
    final imageUrl = await ref.getDownloadURL();
    await sendMessage(chatUser, imageUrl, Type.image);
  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> getUserInfo(
      ChatUser chatUser) {
    return firestore
        .collection('users')
        .where('id', isEqualTo: chatUser.id)
        .snapshots();
  }

  // update online or last active status of user
  static Future<void> updateActiveStatus(bool isOnline) async {
    firestore.collection('users').doc(user.uid).update({
      'is_online': isOnline,
      'last_active': DateTime.now().millisecondsSinceEpoch.toString(),
      'push_token': me.pushToken,
    });
  }

  //delete message
  static Future<void> deleteMessage(Message message) async {
    await firestore
        .collection('chats/${getConversationID(message.toId)}/messages/')
        .doc(message.sent)
        .delete();

    if (message.type == Type.image) {
      await storage.refFromURL(message.msg).delete();
    }
  }

  //update message
  static Future<void> updateMessage(Message message, String updatedMsg) async {
    await firestore
        .collection('chats/${getConversationID(message.toId)}/messages/')
        .doc(message.sent)
        .update({'msg': updatedMsg});
  }

 static Future<void> deleteMessageById(
      String conversationId, String messageId) async {
    try {
      // Hapus pesan dari Firebase berdasarkan ID pesan
      await firestore
          .collection('chats/$conversationId/messages/')
          .doc(messageId)
          .delete();
    } catch (e) {
      log('deleteMessageById Error: $e');
    }
  }

 static Future<void> sendChatFile(
      ChatUser chatUser, String filePath, String filename) async {
    try {
      final ext = filePath.split('.').last;

      // Membuat referensi ke Firebase Storage dengan path yang unik
      final storageRef = storage.ref().child(
          'chat_files/${getConversationID(chatUser.id)}/${DateTime.now().millisecondsSinceEpoch}.$ext');

      // Mengunggah file ke Firebase Storage
      final File file = File(filePath);
      final UploadTask uploadTask = storageRef.putFile(
        file,
        SettableMetadata(contentType: 'application/$ext'),
      );

      final TaskSnapshot snapshot = await uploadTask.whenComplete(() {});

      if (snapshot.state == TaskState.success) {
        final String downloadUrl = await storageRef.getDownloadURL();

        // Mengirim pesan dengan tipe file
        await sendMessage(chatUser, downloadUrl, Type.file,
            filename: filename); // Sertakan filename
      } else {
        // Terjadi kesalahan saat mengunggah file
        print('Error uploading file.');
      }
    } catch (e) {
      print('Error sending file: $e');
    }
  }

   static Future<void> deleteConversation(ChatUser user) async {
    try {
      final conversationId = getConversationID(user.id);
      final conversationRef = firestore.collection('chats').doc(conversationId);

      // Hapus dokumen percakapan
      await conversationRef.delete();
      print('Percakapan dengan ${user.name} berhasil dihapus.');
    } catch (e) {
      print('Gagal menghapus percakapan: $e');
    }
  }

  // Hapus pesan dari seorang pengguna
  static Future<void> deleteMessages(ChatUser user) async {
    try {
      final conversationId = getConversationID(user.id);
      final messagesRef =
          firestore.collection('chats/$conversationId/messages');

      // Dapatkan daftar dokumen dalam koleksi pesan
      final querySnapshot = await messagesRef.get();

      // Hapus setiap dokumen pesan
      for (final doc in querySnapshot.docs) {
        await doc.reference.delete();
      }

      print('Pesan dari ${user.name} berhasil dihapus.');
    } catch (e) {
      print('Gagal menghapus pesan: $e');
    }
  }

  static Future<Member?> getChatUser(String email) async {
  final data = await firestore
      .collection('users')
      .where('email', isEqualTo: email)
      .get();

  if (data.docs.isNotEmpty) {
    final userData = data.docs.first.data();
    final email = userData['email'] as String;
    final name = userData['name'] as String;
    final image = userData['image'] as String;

    return Member(email, name, image);
  } else {
    return null; // Mengembalikan null jika pengguna tidak ditemukan
  }
}


}
