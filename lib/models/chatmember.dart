class ChatMember {
  final String userId; 
  final String name;
  final String email;
  final String image;

  ChatMember({
    required this.userId,
    required this.name,
    required this.email,
    required this.image,
  });
}
