
class Message {
  final String toId;
  final String msg;
  final String read;
  final Type type;
  final String fromId;
  final String sent;
  String? fileUrl;
  String? fileName;
  late bool isDownloaded;
  String? localFilePath; 

  Message({
    required this.toId,
    required this.msg,
    required this.read,
    required this.type,
    required this.fromId,
    required this.sent,
    this.fileUrl,
    this.fileName,
    required this.isDownloaded, 
    this.localFilePath,
  });

   void setLocalFilePath(String? path) {
    localFilePath = path;
  }

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      toId: json['toId'].toString(),
      msg: json['msg'].toString(),
      read: json['read'].toString(),
      type: _getTypeFromString(json['type'].toString()),
      fromId: json['fromId'].toString(),
      sent: json['sent'].toString(),
      fileUrl: json['fileUrl']?.toString(),
      fileName: json['fileName']?.toString(),
      isDownloaded:json['isDownloaded'] ?? false, 
    );
  }


  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['toId'] = toId;
    data['msg'] = msg;
    data['read'] = read;
    data['type'] = type.name;
    data['fromId'] = fromId;
    data['sent'] = sent;
    data['fileUrl'] = fileUrl;
    data['fileName'] = fileName;
    data['isDownloaded'] = isDownloaded;
    return data;
  }

  static Type _getTypeFromString(String typeString) {
    switch (typeString) {
      case 'text':
        return Type.text;
      case 'image':
        return Type.image;
      case 'file':
        return Type.file;
      case 'deleted': // Tambahkan jenis pesan deleted
        return Type.deleted;
      default:
        return Type.text;
    }
  }
}

enum Type { text, image, file, deleted }
