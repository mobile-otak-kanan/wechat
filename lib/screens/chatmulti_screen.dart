import 'package:flutter/material.dart';
import 'package:we_chat/api/apis.dart';
import 'package:we_chat/models/chat_user.dart';
import 'package:we_chat/models/chatmember.dart';
import 'package:we_chat/screens/addmember_screen.dart';

class ChatMultiPage extends StatefulWidget {
  final ChatUser user;
  final String chatId;

  ChatMultiPage({required this.user, required this.chatId});

  @override
  _ChatMultiPageState createState() => _ChatMultiPageState();
}

class _ChatMultiPageState extends State<ChatMultiPage> {
  List<ChatMember> chatMembers = [];

  @override
  void initState() {
    super.initState();
    getChatMembers();
  }

  Future<void> getChatMembers() async {
    try {
      // Gantilah dengan logika untuk mengambil anggota obrolan dari Firestore
      // Misalnya, Anda dapat mengambil daftar ID anggota dari dokumen obrolan dan kemudian mengambil detail anggota dari koleksi pengguna.
      // chatMembers = await fetchChatMembers(widget.chatId);
      setState(() {});
    } catch (e) {
      // Handle error
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat Multi'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddMemberScreen(),
                ),
              );
            },
            icon: Icon(Icons.group),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
              bottom: 8.0,
              left: 16.0,
              right: 16.0,
            ),
            child: Container(
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
                color: Colors.grey.shade300,
              ),
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 30,
                    backgroundImage: NetworkImage(widget.user.image),
                  ),
                  SizedBox(width: 16),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        APIs.me.name,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      SizedBox(height: 8),
                      ElevatedButton(
                        onPressed: () {
                          // Tambahkan aksi untuk tombol Pesan di sini
                        },
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                            horizontal: 32,
                            vertical: 10,
                          ),
                        ),
                        child: Text(
                          'Pesan',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.red,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.info, color: Colors.white),
                        SizedBox(width: 8),
                        Text(
                          'Tautkan Akun ke Whatsapp!',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Untuk mengakses status aktivitas',
                      style: TextStyle(
                        color: Colors.yellow,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 16.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'My Rooms :',
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
          ),
          // Daftar anggota obrolan
          Expanded(
            child: ListView.builder(
              itemCount: chatMembers.length,
              itemBuilder: (context, index) {
                final member = chatMembers[index];
                return ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(member.image),
                  ),
                  title: Text(member.name),
                  subtitle: Text(member.email),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
