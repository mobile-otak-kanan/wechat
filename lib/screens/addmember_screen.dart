import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:we_chat/api/apis.dart'; // Pastikan Anda telah mengimpor API yang diperlukan.
import 'package:we_chat/helper/dialogs.dart';

class AddMemberScreen extends StatefulWidget {
  @override
  _AddMemberScreenState createState() => _AddMemberScreenState();
}

class _AddMemberScreenState extends State<AddMemberScreen> {
  final TextEditingController _emailController = TextEditingController();

  Future<void> _addMemberDialog() async {
    String email = '';

    await showDialog(
      context: context,
      builder: (_) => AlertDialog(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: Row(
          children: const [
            Icon(
              Icons.person_add,
              color: Colors.black,
              size: 24,
            ),
            Text(
              '  Tambahkan pengguna',
              style: TextStyle(
                fontSize: 18,
              ),
            )
          ],
        ),
        content: Container(
          width: 310.0,
          height: 60,
          child: TextFormField(
            maxLines: null,
            onChanged: (value) => email = value,
            decoration: InputDecoration(
              hintText: 'Email Id',
              hintStyle: TextStyle(fontSize: 16),
              prefixIcon: const Icon(Icons.email, color: Colors.black),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ),
        actions: [
          MaterialButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text(
              'Batal',
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
          ),
          MaterialButton(
            onPressed: () async {
              Navigator.pop(context);
              if (email.isNotEmpty) {
                await _addMemberToFirestore(email);
              }
            },
            child: const Text(
              'Tambah',
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
          )
        ],
      ),
    );

  }

  Future<void> _addMemberToFirestore(String email) async {
    // Periksa apakah anggota sudah ada sebelum menambahkannya
    final isMemberExist = await _checkMemberExist(email);

    if (!isMemberExist) {
      final member = await APIs.getChatUser(email);
      if (member != null) {
        // Mendapatkan referensi ke dokumen pengguna saat ini
        final currentUserDocRef =
            FirebaseFirestore.instance.collection('users').doc(APIs.user.uid);

        // Menambahkan anggota ke dalam subkoleksi "members"
        await currentUserDocRef.collection('members').add({
          'email': member.email,
          'name': member.name,
          'image': member.image,
        });
      } else {
        Dialogs.showSnackBar(context, 'Pengguna tidak ditemukan !');
      }
    } else {
      Dialogs.showSnackBar(context, 'Anggota sudah ada!');
    }
  }

  // Fungsi untuk memeriksa apakah anggota sudah ada
  Future<bool> _checkMemberExist(String email) async {
    final currentUserDocRef =
        FirebaseFirestore.instance.collection('users').doc(APIs.user.uid);

    final memberQuery = await currentUserDocRef
        .collection('members')
        .where('email', isEqualTo: email)
        .get();

    return memberQuery.docs.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tambah Anggota'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('users') // Gunakan koleksi 'users'
            .doc(APIs.user.uid) // Dokumen pengguna saat ini
            .collection('members') // Subkoleksi 'members' yang terkait
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          } else if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
            return Center(
              child: Text('Belum ada anggota'),
            );
          }

          final membersData = snapshot.data!.docs;

          return ListView.builder(
            itemCount: membersData.length,
            itemBuilder: (context, index) {
              final member = membersData[index].data() as Map<String, dynamic>;
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(member['image']),
                      backgroundColor: Colors.grey,
                    ),
                    title: Text(
                      member['name'],
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(member['email']),
                    trailing: IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () async {
                        await FirebaseFirestore.instance
                            .collection('users') // Gunakan koleksi 'users'
                            .doc(APIs.user.uid) // Dokumen pengguna saat ini
                            .collection(
                                'members') // Subkoleksi 'members' yang terkait
                            .doc(membersData[index]
                                .id) // Dokumen anggota yang ingin dihapus
                            .delete();
                      },
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: FloatingActionButton(
          onPressed: () {
            _addMemberDialog();
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }
}

class Member {
  final String email;
  final String name;
  final String image;

  Member(this.email, this.name, this.image);
}
